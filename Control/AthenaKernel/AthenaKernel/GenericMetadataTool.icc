///////////////////////// -*- C++ -*- /////////////////////////////

/*
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
*/

// Implementation file for class GenericMetadataTool
// Authors: Jack Cranshaw <Jack.Cranshaw@cern.ch>
///////////////////////////////////////////////////////////////////

// STL include
#include <algorithm>

#include "AthenaKernel/MetaCont.h"
#include "AthenaKernel/ClassID_traits.h"
#include "AthenaKernel/errorcheck.h"
#include "StoreGate/WriteMetaHandle.h"
#include "AthenaBaseComps/AthCheckMacros.h"
#include "AthContainersInterfaces/IConstAuxStoreMeta.h"

template <typename T, typename U>
GenericMetadataTool<T,U>::GenericMetadataTool(const std::string& type, 
                               const std::string& name,
                               const IInterface*  parent)
  : AthAlgTool(type,name,parent),
    m_inputMetaStore( "StoreGateSvc/InputMetaDataStore", name ),
    m_outputMetaStore( "StoreGateSvc/MetaDataStore", name ),
    m_processMetadataTaken(false),
    m_markIncomplete(true)
{
  declareProperty("OutputCollName", m_outputCollName="GenericMetadataOutput",  
    "The default name of the container for output files");
  declareProperty("InputCollName", m_inputCollName = "GenericMetadataInput",
    "The default name of the container for input files");
  declareProperty("ProcessMetadataCollName", m_procMetaName = "ProcessMetadata",
    "The default name of the container for process meta");
  declareProperty("MarkIncomplete", m_markIncomplete = true, 
    "Defaults to filling both complete and incomplete bookkeepers");
  declareInterface< ::IMetaDataTool >( this );
}



template <typename T, typename U>
GenericMetadataTool<T,U>::~GenericMetadataTool()
{
}



template <typename T, typename U>
StatusCode
GenericMetadataTool<T,U>::initialize()
{
  ATH_MSG_DEBUG( "Initializing " << name() << " - package version " << PACKAGE_VERSION );

  ATH_MSG_DEBUG("InputCollName = " << m_inputCollName);
  ATH_MSG_DEBUG("OutputCollName = " << m_outputCollName);
  ATH_MSG_DEBUG("ProcessMetadataCollName = " << m_procMetaName);

  return StatusCode::SUCCESS;
}



template <typename T, typename U>
StatusCode GenericMetadataTool<T,U>::beginInputFile(const SG::SourceID& sid)
{
  ATH_MSG_DEBUG("beginInputFile " << this->name() << "\n" << outputMetaStore()->dump());
  //OPENING NEW INPUT FILE
  //Things to do:
  // 1) note that a file is currently opened
  // 2) Load metadata T from input file
  //    2a) if incomplete from input, directly propagate to output
  //    2b) if complete from input, wait for EndInputFile to decide what to do in output

  const std::string storename("MetaDataStore+");
  if (m_inputCollName != "") {  // are there inputs
    //  IF NO METACONT IN OUTPUT STORE YET
    //     Initialize MetaCont for incomplete and tmp containers in output store
    //
    std::string tmp_name = storename+m_outputCollName+"tmp";
    ATH_CHECK(buildAthenaInterface(m_inputCollName,tmp_name,sid));

    // Do the following if we want incomplete processings marked
    if (m_markIncomplete) {
      std::string inc_name = storename+"Incomplete"+m_outputCollName; 
      std::string input_inc_name = "Incomplete"+m_inputCollName;
      ATH_CHECK(buildAthenaInterface(input_inc_name,inc_name,sid));
    }
  } // inputCollName if
  else {
    ATH_MSG_INFO("No input name");
  }

  // reset process metadata taken marker for new input
  m_processMetadataTaken = false;

  // mark that a new source id has been opened
  m_read.insert(sid);
  
  return StatusCode::SUCCESS;
}


template <typename T, typename U>
StatusCode GenericMetadataTool<T,U>::endInputFile(const SG::SourceID& sid)
{
  // Add the sid to the list of complete sids
  if (m_inputCollName != "") {  // are there inputs
    m_fullreads.insert(sid);
  }

  return StatusCode::SUCCESS;
}

template <typename T, typename U>
StatusCode GenericMetadataTool<T,U>::metaDataStop()
{
  const std::string storename("MetaDataStore+");
  if (m_inputCollName != "") {  // are there inputs
    //TERMINATING THE JOB (EndRun)
    //Things to do:
    // 1) Create new incomplete metadata T if relevant
    // 2) Print cut flow summary
    // 3) Write root file if requested
    // Now retrieve pointers for the MetaConts
    std::string tmp_name = storename+m_outputCollName+"tmpCont";
    const MetaCont<T>* tmp;
    ATH_CHECK(outputMetaStore()->retrieve(tmp,tmp_name));
    T* outcom = new T();
    U* outcom_aux = new U();             // AUX PART
    outcom->setStore(outcom_aux);        // AUX PART

    if (m_markIncomplete) {
      std::string inc_name = storename+"Incomplete"+m_outputCollName+"Cont"; 
      // Build incomplete container to fill
      T* outinc = new T();
      U* outinc_aux = new U();           // AUX PART
      outinc->setStore(outinc_aux);      // AUX PART
      // Check if there were any incomplete inputs
      const MetaCont<T>* inc;
      if(outputMetaStore()->retrieve(inc,inc_name).isSuccess()) {

        // Incomplete inputs can just be merged
        auto sids_inc = inc->sources();
        T* contptr(nullptr);
        for (auto it = sids_inc.begin(); it != sids_inc.end(); ++it) {
          if (!inc->find(*it,contptr)) {
            ATH_MSG_ERROR("Container sid list did not match contents");
          } else {
            ATH_CHECK(updateContainer(outinc,contptr));
          }
          contptr = nullptr; 
        }
      } else {
        ATH_MSG_INFO("Did not find MetaCont for " << inc_name << ", assuming input had no incomplete bookkeepers");
      }

      // Loop over containers and mark based on end files seen
      auto sids_tmp = tmp->sources();
      T* contptr(nullptr);
      for (auto it = sids_tmp.begin(); it != sids_tmp.end(); ++it) {
        if (!tmp->find(*it,contptr)) {
          ATH_MSG_ERROR("Container sid list did not match contents");
        } else {
          bool complete = std::find(m_fullreads.begin(),
                       m_fullreads.end(),
                       *it) != m_fullreads.end();
          bool not_written = std::find(m_written.begin(),
                          m_written.end(),
                          *it) == m_written.end();
          if (complete && not_written) {
            ATH_CHECK(updateContainer(outcom,contptr));
          } else {
            ATH_CHECK(updateContainer(outinc,contptr));
          }
        }
      }

      std::string incout_name = "Incomplete"+m_outputCollName;
      // Do any cleanup
      if (outputMetaStore()->contains(ClassID_traits<T>::ID(),incout_name) ) {
        ATH_MSG_INFO("Cleaning up class for " << incout_name);
        const T* tmpBook(nullptr);
        if ( outputMetaStore()->retrieve(tmpBook,incout_name).isSuccess() ) {
          const SG::IConstAuxStore* tmpBookAux = tmpBook->getConstStore(); // AUX PART
          ATH_CHECK(outputMetaStore()->removeDataAndProxy(tmpBook));
          ATH_CHECK(outputMetaStore()->removeDataAndProxy(tmpBookAux));    // AUX PART
        }
        else ATH_MSG_INFO("StoreGate failed retrieve");
      }
      ATH_CHECK(outputMetaStore()->record(outinc,incout_name));
      ATH_CHECK(outputMetaStore()->record(outinc_aux,incout_name+"Aux.")); // AUX PART
    }  // markIncomplete
    else {
      auto sids_tmp = tmp->sources();
      T* contptr(nullptr);
      // just merge complete inputs into complete/output container
      for (auto it = sids_tmp.begin(); it != sids_tmp.end(); ++it) {
        if (!tmp->find(*it,contptr)) {
          ATH_MSG_ERROR("Container sid list did not match contents");
        } else {
          // default to not worrying about marking
          ATH_CHECK(updateContainer(outcom,contptr));
        }
      }
    }

    // Record container objects directly in store for output
    if (outputMetaStore()->contains(ClassID_traits<T>::ID(),m_outputCollName)) {
      ATH_MSG_INFO("Cleaning up class for " << m_outputCollName);
      const T* tmpBook(nullptr);
      if ( outputMetaStore()->retrieve(tmpBook,m_outputCollName).isSuccess() ) {
        const SG::IConstAuxStore* tmpBookAux = tmpBook->getConstStore();  // AUX PART
        ATH_CHECK(outputMetaStore()->removeDataAndProxy(tmpBook));
        ATH_CHECK(outputMetaStore()->removeDataAndProxy(tmpBookAux));     // AUX PART
      }
      else ATH_MSG_ERROR("StoreGate failed retrieve");
    }
    ATH_CHECK(outputMetaStore()->record(outcom,m_outputCollName));
    ATH_CHECK(outputMetaStore()->record(outcom_aux,m_outputCollName+"Aux.")); // AUX PART
  } // inputCollName if

  if (!m_processMetadataTaken) {
    if (addProcessMetadata().isFailure()) {
      ATH_MSG_ERROR("Could not add process metadata");
    }
  }
  else {
    ATH_MSG_DEBUG("Process metadata written into container before metaDataStop");
  }

  // Reset after metadata stop
  m_processMetadataTaken = false;

  if (m_inputCollName != "") {  // are there inputs
    // Copy read files into written files
    for (auto it = m_read.begin(); it != m_read.end(); ++it) {
      m_written.insert(*it);
    }
    // Remove completes from read
    for (auto it = m_fullreads.begin(); it != m_fullreads.end(); ++it) {
      m_read.erase(*it);
    }
    m_fullreads.clear();
  } // inputCollName if
  
  return StatusCode::SUCCESS;
}


template <typename T, typename U>
StatusCode
GenericMetadataTool<T,U>::finalize()
{
  ATH_MSG_DEBUG( "Finalizing " << name() << " - package version " << PACKAGE_VERSION );
  return StatusCode::SUCCESS;
}


template <typename T, typename U>
StatusCode GenericMetadataTool<T,U>::initOutputContainer( const std::string& sgkey)
{
  std::string key = sgkey;
  // Create the primary container
  // Put it in a MetaCont
  MetaCont<T>* mcont = new MetaCont<T>(DataObjID(ClassID_traits<T>::ID(),key));
  // Do the same for the auxiliary container
  std::string auxkey(key+"Aux.");    // AUX PART
  DataObjID obj(ClassID_traits<SG::IConstAuxStore>::ID(),auxkey); // AUX PART
  MetaCont<SG::IConstAuxStore>* acont = new MetaCont<SG::IConstAuxStore>(obj); // AUX PART 
  ATH_CHECK(outputMetaStore()->record(std::move(mcont),key));
  ATH_CHECK(outputMetaStore()->record(std::move(acont),auxkey));  // AUX PART
  // Create a symlink
  // so that proxy references to <Aux>#auxkey find it in MetaCont<Aux>#auxkey
  try {
  StatusCode sc = outputMetaStore()->symLink
          (
            ClassID_traits<MetaCont<SG::IConstAuxStore> >::ID(),
            auxkey,
            ClassID_traits<SG::IConstAuxStore>::ID()
          );
    if (sc.isFailure()) {
      ATH_MSG_ERROR("Unable to symlink " << auxkey);
    }
  }
  catch (std::exception& e) {
    ATH_MSG_INFO("SYMLINK EXCEPTION " << e.what());
  }

  return StatusCode::SUCCESS;
}

//--------------------------------------------------------//
//  MetaConts are only needed when reading in Athena
//  This builds them and populates them with bookeepers from the input store
//--------------------------------------------------------//
template <typename T, typename U>
StatusCode GenericMetadataTool<T,U>::buildAthenaInterface(const std::string& inputName,
                                                const std::string& outputName,
                                                const SG::SourceID& sid)
{
  ATH_MSG_DEBUG("buildAthenaInterface " << inputName << " " << outputName);
  // Make sure the MetaCont is ready in the output store for outputName
  //   If not, then create it
  std::string name = outputName+"Cont"; 
  if( !(outputMetaStore()->contains(ClassID_traits<MetaCont<T> >::ID(),name)) ) {
    ATH_CHECK(this->initOutputContainer(name));
  }
  else {
    ATH_MSG_WARNING("incomplete collection already exists");
  }

  // Retrieve pointer for the MetaCont
  MetaCont<T>* mc;
  ATH_CHECK(outputMetaStore()->retrieve(mc,name));

  // Make sure sid does not already exist in the MetaCont
  for (auto it = mc->sources().begin(); it != mc->sources().end(); ++it) {
    if (*it==sid) {
      ATH_MSG_WARNING("Metadata already exists for sid " << sid);
      return StatusCode::SUCCESS;   // Assume success if sid already exists
    }
  }

  // Get the input container from the input metadata store 
  const T* cbc;
  if (inputMetaStore()->contains(ClassID_traits<T>::ID(),inputName) ) {
    StatusCode ssc = inputMetaStore()->retrieve( cbc, inputName );
    if (ssc.isSuccess()) {
      //Insert input container into MetaCont for this sid 
      T* tostore = new T(*cbc);
      if ( !mc->valid(sid) ) {
        if ( !mc->insert(sid,tostore) ) {
          ATH_MSG_ERROR("Unable to insert " << inputName << " for " << sid << " with key " << name);
          return StatusCode::FAILURE;   // Fail if insert to mc fails
        }
      } else {
        ATH_MSG_WARNING("SID="<<sid<<" already in container");
      }
    }
    else {
      ATH_MSG_ERROR("Could not retrieve class with name " << inputName << " in input store");
      return StatusCode::FAILURE;   // Fail if store contains, but not retrieves
    }
  }
  else {
    ATH_MSG_WARNING("No " << inputName << " data in this file ");
  }

  return StatusCode::SUCCESS;
}

template <typename T, typename U>
StatusCode GenericMetadataTool<T,U>::addProcessMetadata()
{
  // Add the information from the current processing to the complete output
  // --> same paradigm as original CutFlowSvc
  // Get the complete bookkeeper collection of the output meta-data store
  T* completeBook(nullptr); 
  if( !(outputMetaStore()->retrieve( completeBook, m_outputCollName) ).isSuccess() ) {
    ATH_MSG_ERROR( "Could not get output container from output MetaDataStore" );
    return StatusCode::FAILURE;
  }

  // Get the bookkeeper from the current processing
  T* fileCompleteBook(nullptr);
  if( outputMetaStore()->contains(ClassID_traits<T>::ID(),m_procMetaName) ) {
    if( !(outputMetaStore()->retrieve( fileCompleteBook, m_procMetaName) ).isSuccess() ) {
      ATH_MSG_WARNING( "Could not get process metadata from output MetaDataStore" );
    }
    else {
      // update the complete output with the complete input
      ATH_CHECK(this->updateContainer(completeBook,fileCompleteBook));
    }
  }
  else {
    ATH_MSG_INFO("No process container " << m_procMetaName);
  }

  return StatusCode::SUCCESS;
}


