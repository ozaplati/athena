/*
  Copyright (C) 2002-2018 CERN for the benefit of the ATLAS collaboration
*/

#ifndef IdentifierSet_h
#define IdentifierSet_h

#include <set>
#include "Identifier/Identifier.h"

typedef std::set<Identifier> IdentifierSet;

// Class definition
#include "AthenaKernel/CLASS_DEF.h"
CLASS_DEF( IdentifierSet , 229132278 , 1 )

#endif // IdentifierSet_h
